//
//  LoginPage.swift
//  Clime
//
//  Created by bhavin joshi on 06/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class LoginPage: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var uvwlogin: UIView!
    @IBOutlet weak var btnsignup: UIButton!
    @IBOutlet weak var btnlogin: UIButton!
    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var toview_height: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Device.DeviceType.IS_IPHON_X {
            self.toview_height.constant = 220;
        }
        self.uvwlogin.layer.cornerRadius = 5
        self.uvwlogin.layer.masksToBounds = true
        self.uvwlogin.ShadowEffect()
        
        self.txtemail.delegate = self
        self.txtpassword.delegate = self

        // Do any additional setup after loading the view.
    }
    
    // MARK: - textfield delegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtemail{
            self.txtemail.resignFirstResponder()
            self.txtpassword.becomeFirstResponder()
        }
        else{
            self.txtpassword.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - webapi call
    
    func Login(){
        let email = self.txtemail.text
        let password = self.txtpassword.text
        
        
        let emailval = Validation().email(email!)
        if(emailval[0] as? String == "false"){
            Utils().popDialog(controller: self, title: "Email", message: (emailval[1] as! String))
            return
        }
        
        if (password?.isEmpty)! {
            Utils().popDialog(controller: self, title: "Password", message:Constants.validpassword)
            return
        }

        let parameter : [String : Any] = [
            "email":email!,
            "password":password!,
            "device_type":"1",
            "device_token":"123"
        ]
        let MainParamer : [String : Any] = [
            "data":parameter
        ]
        
        Utils().ShowLoader()
        WebService.postMethod(params: MainParamer as [String : AnyObject], apikey: Constants.CheckLogin, completion: { (Json) in
            let datadict = Json as! NSDictionary
            let message = datadict.value(forKey: "message") as? String
            let status = datadict.value(forKey: "status") as? String
            Utils().HideLoader()
            if status == "1"{
                 Constants.USERDEFAULTS.set(datadict["data"], forKey: "LoggedInUserDict")
                self.txtemail.text = ""
                self.txtpassword.text = ""
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let HomePage = storyboard.instantiateViewController(withIdentifier: "HomePage") as? HomePage
                self.navigationController?.pushViewController(HomePage ?? UIViewController(), animated: true)
            }
            else{
                Utils().popDialog(controller: self, title: "Clime", message: message!)
            }
            
        }, failure: {(error) in
            
        })
    
    }
    
    
    // MARK: - button clicks
    @IBAction func btnLoginclick(_ sender: Any) {
        self.Login()
    }
    @IBAction func btnSignupclick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SignUpPage = storyboard.instantiateViewController(withIdentifier: "signUpPage") as? signUpPage
        SignUpPage?.fromLoginPage = "1"
        self.navigationController?.pushViewController(SignUpPage ?? UIViewController(), animated: true)
    }
    
    @IBAction func btnforgotpwdclick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let forgotpassword = storyboard.instantiateViewController(withIdentifier: "ForgotPassword") as? ForgotPassword
        self.navigationController?.pushViewController(forgotpassword ?? UIViewController(), animated: true)
    }
    
}
extension UIView {
    func ShadowEffect(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
    }
}
