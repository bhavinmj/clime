//
//  signUpPage.swift
//  Clime
//
//  Created by bhavin joshi on 06/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class signUpPage: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var uvwsignup: UIView!
    @IBOutlet weak var btnlogin: UIButton!
    @IBOutlet weak var btnnext: UIButton!
    @IBOutlet weak var txtconfpassword: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtclimecode: UITextField!
    @IBOutlet weak var lblCodeHint: UILabel!
    @IBOutlet weak var topview_height: NSLayoutConstraint!
    
    var ShowHint = Bool()
    var fromLoginPage = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        if Device.DeviceType.IS_IPHON_X {
            self.topview_height.constant = 220;
        }
        self.txtemail.delegate = self
        self.txtpassword.delegate = self
        self.txtconfpassword.delegate = self
        ShowHint = false
        self.lblCodeHint.isHidden = true
        
        self.uvwsignup.layer.cornerRadius = 5
        self.uvwsignup.layer.masksToBounds = true
        self.uvwsignup.ShadowEffect()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - textfield delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtemail{
            self.txtemail.resignFirstResponder()
            self.txtpassword.becomeFirstResponder()
        }
        else if textField == self.txtpassword{
            self.txtpassword.resignFirstResponder()
            self.txtconfpassword.becomeFirstResponder()
        }
        else{
            self.txtconfpassword.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Button clicks
    @IBAction func btnLoginclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHintClick(_ sender: Any) {
        if ShowHint{
            self.lblCodeHint.isHidden = true
            ShowHint = false
        }
        else{
            self.lblCodeHint.isHidden = false
            ShowHint = true
        }
    }
    @IBAction func btnNextclick(_ sender: Any) {
        
        
        
        
        let email = self.txtemail.text
        let password = self.txtpassword.text
        let confpass = self.txtconfpassword.text
        
        let emailval = Validation().email(email!)
        if(emailval[0] as? String == "false"){
            Utils().popDialog(controller: self, title: "Email", message: (emailval[1] as! String))
            return
        }
        
        if (password?.isEmpty)! {
             Utils().popDialog(controller: self, title: "Password", message: Constants.validpassword)
            return
        }
        
        if (confpass?.isEmpty)! {
            Utils().popDialog(controller: self, title: "Password", message: Constants.validConfirmpassword)
            return
        }
        
        if confpass != password {
            Utils().popDialog(controller: self, title: "Password", message: Constants.samepassword)
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let PolicyPage = storyboard.instantiateViewController(withIdentifier: "PolicyPage") as? PolicyPage
        PolicyPage?.strEmail = email!
        PolicyPage?.strPassword = password!
        PolicyPage?.fromLoginPage = self.fromLoginPage
        self.navigationController?.pushViewController(PolicyPage ?? UIViewController(), animated: true)

    }
    

}
