//
//  PathPage.swift
//  Clime
//
//  Created by bhavin joshi on 13/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit
class PathPage: UIViewController {
    @IBOutlet weak var scrollview_ypostion: NSLayoutConstraint!
    @IBOutlet weak var uvwtopview_height: NSLayoutConstraint!
    @IBOutlet weak var uvwChngepath: UIView!
    @IBOutlet weak var btnmenu_yposition: NSLayoutConstraint!
    @IBOutlet weak var clwPath_Height: NSLayoutConstraint!
    var arrPathList = NSMutableArray()
    @IBOutlet weak var clwPthList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if Device.DeviceType.IS_IPHON_X{
            self.btnmenu_yposition.constant = 50
            self.scrollview_ypostion.constant = -50
            self.uvwtopview_height.constant = 275
        }
        
        self.uvwChngepath.layer.borderWidth = 1
        self.uvwChngepath.layer.borderColor = UIColor.white.cgColor
        self.uvwChngepath.layer.cornerRadius = self.uvwChngepath.frame.height / 2
        self.uvwChngepath.layer.masksToBounds = true
        
        self.arrPathList = ["As-Usual-Path","Naturalisa Path","Venus Path","Medusa Path","As-Usual-Path","Naturalisa Path","Venus Path","Medusa Path"]
        if self.arrPathList.count % 2 == 0{
            self.clwPath_Height.constant = CGFloat((self.arrPathList.count / 2) * 145)
        }
        else{
            self.clwPath_Height.constant = CGFloat((self.arrPathList.count / 2 + 1) * 145)
        }
        
        self.clwPthList.delegate = self
        self.clwPthList.dataSource = self
        self.clwPthList.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - popup menu button click
    @IBAction func btnMenuclick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ProductGoodCondition = storyboard.instantiateViewController(withIdentifier: "Popupmenu") as? Popupmenu
        
        ProductGoodCondition? .setPopinTransitionStyle(BKTPopinTransitionStyle.slide)
        ProductGoodCondition? .setPopinTransitionDirection(BKTPopinTransitionDirection.top)
        
        let presentationRect = CGRect.init(x:-20, y: -20, width: UIScreen.main.bounds.size.width + 40 , height: 350.0).offsetBy(dx: 0.0, dy: 0.0)
        self.presentPopinController(ProductGoodCondition, from: presentationRect, animated: true, completion: {() -> Void in
            print("Popin presented !")
        })
    }
    
}
extension PathPage : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    // MARK: - collectionview delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPathList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PathCell", for: indexPath) as! PathCell
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        cell.lblpathname.text = self.arrPathList.object(at: indexPath.row) as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let size = CGSize(width: self.clwPthList.frame.size.width / 2 - 5, height: 140)
        //let size = CGSize(width: self.clwProduct.frame.size.width / 2 - 10, height: ItemNamesize + 189)
        return size
        
    }
}
