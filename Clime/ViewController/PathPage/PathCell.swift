//
//  PathCell.swift
//  Clime
//
//  Created by bhavin joshi on 13/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class PathCell: UICollectionViewCell {
    @IBOutlet weak var imgPath: UIImageView!
    
    @IBOutlet weak var lblpathname: UILabel!
}
