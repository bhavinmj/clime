//
//  PolicyPage.swift
//  Clime
//
//  Created by bhavin joshi on 22/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit
import WebKit

class PolicyPage: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var topview_height: NSLayoutConstraint!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var wkPolicy: WKWebView!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnagree: UIButton!
    var agree = Bool()
    var strEmail = String()
    var strPassword = String()
    var fromLoginPage = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(Device.DeviceType.IS_IPHON_X){
            self.topview_height.constant = 160
        }
        wkPolicy.uiDelegate = self as? WKUIDelegate
        Utils().ShowLoader()
        let url = URL(string: "http://jobandoffers.com/demo/clime/policy")
        wkPolicy.navigationDelegate = self as WKNavigationDelegate
        wkPolicy.load(URLRequest(url: url!))
        
        agree = false
        
        // Do any additional setup after loading the view.
    }
     // MARK: - webview delegate methods
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Utils().HideLoader()
    }
     // MARK: - button click
    @IBAction func btnAgreeClick(_ sender: Any) {
        if agree{
            agree = false
            self.btnagree.setImage(UIImage(named: "select_unchecked"), for: .normal)
        }
        else{
            agree = true
            self.btnagree.setImage(UIImage(named: "select_checked"), for: .normal)
        }
    }
    @IBAction func btnSubmitClick(_ sender: Any) {
        
        if agree
        {
                let parameter : [String : Any] = [
                    "email":self.strEmail,
                    "password":self.strPassword
                ]
                let MainParamer : [String : Any] = [
                    "data":parameter
                ]
        
                Utils().ShowLoader()
                WebService.postMethod(params: MainParamer as [String : AnyObject], apikey: Constants.Registration, completion: { (Json) in
                    let datadict = Json as! NSDictionary
                    let message = datadict.value(forKey: "message") as? String
                    let status = datadict.value(forKey: "status") as? String
        
                    if status == "1"{
                
                        self.DisplayPopup(message: message!)
                    }
                    else{
                        Utils().popDialog(controller: self, title: "Clime", message: message!)
                    }
        
                }, failure: {(error) in
        
                })
        }
        else{
            Utils().popDialog(controller: self, title: "Clime", message: Constants.policyagree)
        }
    
    }
    @IBAction func btnBackclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - PopupDisplay
    func DisplayPopup(message : String){
        let dialog = UIAlertController(title: "Clime", message: message, preferredStyle:UIAlertController.Style.alert)
        let objAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
            UIAlertAction in
            
            if self.fromLoginPage == "1"{
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: LoginPage.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let SignUpPage = storyboard.instantiateViewController(withIdentifier: "LoginPage") as? LoginPage
                self.navigationController?.pushViewController(SignUpPage ?? UIViewController(), animated: true)
            }
           
        }
        dialog.addAction(objAction)
        self.present(dialog, animated: true, completion: nil)
    }
    
    
}
extension WKWebView{
    
}
