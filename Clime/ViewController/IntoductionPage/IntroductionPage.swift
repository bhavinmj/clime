//
//  ViewController.swift
//  Clime
//
//  Created by bhavin joshi on 06/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit
import moa

class IntroductionPage: UIViewController {
    var arrIntrodata = NSMutableArray()
    @IBOutlet weak var pageControl_Yposition: NSLayoutConstraint!
    @IBOutlet weak var pagecontrol: UIPageControl!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnsignUp: UIButton!
    @IBOutlet weak var clwIntro: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        URLCache.shared.removeAllCachedResponses()
        if Device.DeviceType.IS_IPHON_X{
            self.pageControl_Yposition.constant = 240;
        }
        else if Device.DeviceType.IS_IPHONE_6P{
            self.pageControl_Yposition.constant = 280;
        }
        else if Device.DeviceType.IS_IPHONE_6{
            self.pageControl_Yposition.constant = 270;
        }
        self.pagecontrol.currentPage = 0
        self.intropage()
        
    }
    // MARK: - button click
    @IBAction func btnSignUpClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SignUpPage = storyboard.instantiateViewController(withIdentifier: "signUpPage") as? signUpPage
        SignUpPage?.fromLoginPage = "0"
        self.navigationController?.pushViewController(SignUpPage ?? UIViewController(), animated: true)
    }
    @IBAction func btnLoginclick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SignUpPage = storyboard.instantiateViewController(withIdentifier: "LoginPage") as? LoginPage
        self.navigationController?.pushViewController(SignUpPage ?? UIViewController(), animated: true)
    }
    
    // MARK: - webapi calling
    func intropage()
    {
        let MainParamer : [String : Any] = [:]
        Utils().ShowLoader()
        WebService.GetMethod(params: MainParamer as [String : AnyObject], apikey: Constants.Welcome, completion: { (Json) in
            let datadict = Json as! NSDictionary
            let message = datadict.value(forKey: "message") as? String
            let status = datadict.value(forKey: "status") as? String
            Utils().HideLoader()
            if status == "1"{
                let data = datadict.value(forKey: "data") as! NSDictionary
                self.arrIntrodata.addObjects(from: data.object(forKey: "questionsFor_list") as! [Any])
                self.pagecontrol.numberOfPages = self.arrIntrodata.count
                self.clwIntro.delegate = self
                self.clwIntro.dataSource = self
                self.clwIntro.reloadData()
            }
            else{
                Utils().popDialog(controller: self, title: "Clime", message: message!)
            }
            
        }, failure: {(error) in
            
        })
    }

    
    
}
extension IntroductionPage: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    // MARK: - collectionview delegate methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrIntrodata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! IntroCell
        
        //cell.lblContent.text = self.arrContentDetail.object(at: indexPath.row) as? String
        let itemdata = self.arrIntrodata.object(at: indexPath.row) as! NSDictionary
        cell.lblTitle.text = itemdata.value(forKey: "title") as? String
        cell.lblDetail.text = itemdata.value(forKey: "description") as? String
        
        cell.myIndicator.isHidden = false
        cell.myIndicator.startAnimating()
        cell.imgIntro.moa.onSuccess = { image in
            cell.myIndicator.stopAnimating()
            cell.myIndicator.isHidden = true
            return image
        }
        cell.imgIntro.moa.url = itemdata.value(forKey: "image") as? String

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.clwIntro.frame.size.width, height: self.clwIntro.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.pagecontrol?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
}
