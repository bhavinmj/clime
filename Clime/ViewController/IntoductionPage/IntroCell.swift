//
//  IntroCell.swift
//  Clime
//
//  Created by bhavin joshi on 06/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {
    @IBOutlet weak var imgIntro: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var myIndicator: UIActivityIndicatorView!
}
