//
//  ProfilePage.swift
//  Clime
//
//  Created by bhavin joshi on 12/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class ProfilePage: UIViewController {

    @IBOutlet weak var scrollview_yposition: NSLayoutConstraint!
    @IBOutlet weak var uvwPremium: UIView!
    @IBOutlet weak var uvwPath: UIView!
    @IBOutlet weak var uvwInformation: UIView!
    @IBOutlet weak var topview_height: NSLayoutConstraint!
    @IBOutlet weak var btndeleteaccount: UIButton!
    @IBOutlet weak var btnlogout: UIButton!
    @IBOutlet weak var btnPremium: UIButton!
    @IBOutlet weak var lblflavour: UILabel!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtbirthdate: UITextField!
    @IBOutlet weak var txtusername: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        if Device.DeviceType.IS_IPHON_X {
            self.topview_height.constant = 150
            self.scrollview_yposition.constant = -50
        }
        self.uvwInformation.layer.cornerRadius = 5
        self.uvwInformation.layer.masksToBounds = true
        self.uvwInformation.ShadowEffect()
        
        self.uvwPath.layer.cornerRadius = 2
        self.uvwPath.layer.masksToBounds = true
        self.uvwPath.ShadowEffect()
        
        self.uvwPremium.layer.cornerRadius = 2
        self.uvwPremium.layer.masksToBounds = true
        self.uvwPremium.ShadowEffect()
    }
    
    // MARK: - Button clicks
    @IBAction func btnSelectPathclick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let Profile = storyboard.instantiateViewController(withIdentifier: "PathPage") as? PathPage
        self.navigationController?.pushViewController(Profile ?? UIViewController(), animated: true)
    }
    @IBAction func btnMenuClick(_ sender: Any) {
        self.openMenu()
    }
    @IBAction func btnDeleteAccount(_ sender: Any) {

    }
    
    @IBAction func btnLogOutclick(_ sender: Any) {
        //®   self.navigationController?.popViewController(animated: true)
        AppDelegate().SetupMenu()
    }
    
    // MARK: - popup menu
    func openMenu(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ProductGoodCondition = storyboard.instantiateViewController(withIdentifier: "Popupmenu") as? Popupmenu
        
        ProductGoodCondition? .setPopinTransitionStyle(BKTPopinTransitionStyle.slide)
        ProductGoodCondition? .setPopinTransitionDirection(BKTPopinTransitionDirection.top)
        
        let presentationRect = CGRect.init(x:-20, y: -20, width: UIScreen.main.bounds.size.width + 40 , height: 350.0).offsetBy(dx: 0.0, dy: 0.0)
        self.presentPopinController(ProductGoodCondition, from: presentationRect, animated: true, completion: {() -> Void in
            print("Popin presented !")
        })
    }

}
