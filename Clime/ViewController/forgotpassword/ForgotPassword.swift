//
//  ForgotPassword.swift
//  Clime
//
//  Created by bhavin joshi on 22/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var uvwforgotview: UIView!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var topview_height: NSLayoutConstraint!
    
    @IBOutlet weak var btnsubmit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if Device.DeviceType.IS_IPHON_X {
            self.topview_height.constant = 160;
        }
        self.uvwforgotview.layer.cornerRadius = 5
        self.uvwforgotview.layer.masksToBounds = true
        self.uvwforgotview.ShadowEffect()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Button clicks
    func forgotapi(){
        let email = self.txtemail.text
        let emailval = Validation().email(email!)
        if(emailval[0] as? String == "false"){
            Utils().popDialog(controller: self, title: "Email", message: (emailval[1] as! String))
            return
        }
        
        let parameter : [String : Any] = [
            "email":email!
        ]
        let MainParamer : [String : Any] = [
            "data":parameter
        ]
        
        Utils().ShowLoader()
        WebService.postMethod(params: MainParamer as [String : AnyObject], apikey: Constants.ForgotPassword, completion: { (Json) in
            let datadict = Json as! NSDictionary
            let message = datadict.value(forKey: "message") as? String
            let status = datadict.value(forKey: "status") as? String
            Utils().HideLoader()
            if status == "1"{
               self.DisplayPopup(message: message!)
            }
            else{
                Utils().popDialog(controller: self, title: "Clime", message: message!)
            }
            
        }, failure: {(error) in
            
        })
        
    }
    
     // MARK: - Button clicks
    @IBAction func btnsubmitclick(_ sender: Any) {
        self.forgotapi()
    }
    @IBAction func btnbackclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
      // MARK: - PopupDisplay
    func DisplayPopup(message : String){
        let dialog = UIAlertController(title: "Clime", message: message, preferredStyle:UIAlertController.Style.alert)
        let objAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.navigationController?.popViewController(animated: true)
        }
        dialog.addAction(objAction)
        self.present(dialog, animated: true, completion: nil)
    }
    
    
}
