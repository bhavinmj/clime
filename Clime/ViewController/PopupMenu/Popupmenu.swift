//
//  Popupmenu.swift
//  Clime
//
//  Created by bhavin joshi on 13/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class Popupmenu: UIViewController {
    var arrMenu = NSMutableArray()
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var btnclose: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        arrMenu = ["Home","Path","Personal Resources","Account"]
        self.tblMenu.delegate = self
        self.tblMenu.dataSource = self
        self.tblMenu.reloadData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - button click
    @IBAction func btnCloseClick(_ sender: Any) {
        self.presentingPopin().dismissCurrentPopinController(animated: false, completion: {() -> Void in
            
        })
    }
    

}

extension Popupmenu: UITableViewDelegate,UITableViewDataSource{
    // MARK: - tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "Cell")
        cell.textLabel!.text = self.arrMenu.object(at: indexPath.row) as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.presentingPopin().dismissCurrentPopinController(animated: false, completion: {() -> Void in
            
        })
    }
    
    
}
