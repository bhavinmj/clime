//
//  Dashboard.swift
//  Clime
//
//  Created by bhavin joshi on 24/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class Dashboard: UIViewController {

    @IBOutlet weak var clwCooldown: UICollectionView!
    @IBOutlet weak var clwBoostUp: UICollectionView!
    @IBOutlet weak var tblRecommandation: UITableView!
    @IBOutlet weak var uvwRecommandation_height: NSLayoutConstraint!
    @IBOutlet weak var uvwRecommandation: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblRecommandation.delegate = self
        self.tblRecommandation.dataSource = self
        self.tblRecommandation.reloadData()
        
        self.clwBoostUp.delegate = self
        self.clwBoostUp.dataSource = self
        self.clwBoostUp.reloadData()
        
        self.clwCooldown.delegate = self
        self.clwCooldown.dataSource = self
        self.clwCooldown.reloadData()
    }
    


}

extension Dashboard : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Recommanded"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RecommndationCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("RecommndationCell", owner: self, options: nil)
            cell = nib?[0] as? RecommndationCell
        }
        cell?.selectionStyle = .none
        cell?.lblRecommandText.text = "Recommandation \(indexPath.row)"
            return cell!
        }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
}
extension Dashboard: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.clwBoostUp{
              return 3
        }
        else
        {
              return 3
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Boostupcell", for: indexPath) as! Boostupcell
        cell.layer.cornerRadius = 5
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius = 2.0
        cell.layer.masksToBounds = false
         
        if collectionView == self.clwBoostUp{
            cell.lblBoostContent.text = "boost \(indexPath.row)"
        }
        else{
            cell.lblBoostContent.text = "cool down \(indexPath.row)"
        }
        return cell
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
}
