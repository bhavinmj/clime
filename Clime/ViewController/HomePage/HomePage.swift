//
//  HomePage.swift
//  Clime
//
//  Created by bhavin joshi on 10/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import UIKit

class HomePage: UIViewController {

    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var topview_height: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        if Device.DeviceType.IS_IPHON_X{
            self.topview_height.constant = 80;
        }
        
        self.btnLogout.layer.cornerRadius = 10
        self.btnLogout.layer.borderColor = UIColor.black.cgColor
        self.btnLogout.layer.borderWidth = 1.0
        self.btnLogout.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    
    // MARK: - button clicks
    @IBAction func btnLogoutClick(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginPage.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
