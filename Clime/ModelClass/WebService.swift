//
//  WebService.swift
//  Orion
//
//  Created by Dhruv Patel on 26/09/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import Alamofire

class WebService: NSObject {

    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }

    static func jsonToDictionary(from text: String) -> [String: Any]? {
        guard let data = text.data(using: .utf8) else { return nil }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: Any]
    }
    
    class func postMethod (params: [String : AnyObject], apikey: String,completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void)
    {
        if Utils().isConnectedToNetwork() == false{
            
            //Utils().showMessage(Constants.NetworkUnavailable)
             Utils().HideLoader()
            return
        }
        
        print("Parameterv - \(params)")
        
        print(params)
        
        
        let strURL = Constants.BASE_URL + apikey
        let url = URL(string: strURL)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url!, method: .post, parameters: params,encoding: JSONEncoding.default)
            .responseJSON
            {
                response in
                switch (response.result)
                {
                case .success:
                    let jsonResponse = response.result.value as! NSDictionary
                    print(jsonResponse)
                    completion(jsonResponse)
                    Utils().HideLoader()
                case .failure(let error):                    
                   // Utils().showMessage("Something went wrong")
                    failure(error)
                    print(error.localizedDescription)
                    Utils().HideLoader()
                    break
                }
        }
    }
    
    class func GetMethod (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void)
    {
        if Utils().isConnectedToNetwork() == false
        {
            //Utils().showMessage(Constants.NetworkUnavailable)
             Utils().HideLoader()
            return
        }
        
        //Utils().ShowLoader()
        let strURL = Constants.BASE_URL + apikey
        let url = URL(string: strURL)
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(url!, method: .get, parameters: params)
            .responseJSON
            {
                response in
                
                switch (response.result)
                {
                case .success:
                    
                    //do json stuff
                    let jsonResponse = response.result.value as! NSDictionary
                    print(jsonResponse)
                    completion(jsonResponse)
                    
                    
                case .failure(let error):
                    
                   //Utils().showMessage("Something went wrong")
                    failure(error)
                    //Utils().HideLoader()
                    break
                }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
   class func postMultiPartdata (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void){

        if Utils().isConnectedToNetwork() == false
        {
           // Utils().showMessage(Constants.NetworkUnavailable)
             Utils().HideLoader()
            return
        }

        print(params)
    
        Utils().ShowLoader()
        let strURL = Constants.BASE_URL + apikey
        let url = URL(string: strURL)

        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
    

    Alamofire.upload(
        multipartFormData: { MultipartFormData in
            
            for (key, value) in params {
                if value is Data{
                    let imgData = params[key] as! Data
                    MultipartFormData.append(imgData, withName: key, fileName: "productimage.jpg", mimeType: "image/jpeg")
                }
                else{
                     MultipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }

            }
    }, to: url!) { (result) in

        switch (result)
        {
        case .success(let upload, _, _):
            upload.responseJSON { response in
                print("Response :- \(response)")
                let jsonResponse = response.result.value as! NSDictionary
                print(jsonResponse)
                completion(jsonResponse)
                Utils().HideLoader()

            }
        case .failure(let encodingError):
            print(encodingError)
        }
    }
    }

}




