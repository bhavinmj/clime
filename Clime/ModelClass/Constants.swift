//
//  Constants.swift
//  Clime
//
//  Created by bhavin joshi on 06/02/19.
//  Copyright © 2019 bhavin joshi. All rights reserved.
//

import Foundation
import UIKit

class Constants {

    public static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    public static let USERDEFAULTS = UserDefaults.standard
    public static let BASE_URL = "http://jobandoffers.com/demo/clime/api/"
    
    public static let CheckLogin = "Registration/CheckLogin"
    
    public static let Welcome = "Registration/WelcomeScreen"
    public static let Registration = "Registration/UserRegistration"
    public static let GetUserDetail = "Registration/GetUserDetail"
    public static let ForgotPassword = "Registration/ForgotPassword"
    
    
    
    // let IS_IPHONE = (UI_USER_INTERFACE_IDIOM() == .phone)
    public static let NetworkUnavailable = "Network unavailable. Please check your internet connectivity"
    public static let validpassword = "Please enter valid password."
    public static let DialogTitle = "Please enter valid password."
    public static let validConfirmpassword = "Please enter valid confirm password."
    public static let policyagree = "Please agree with terms."
    public static let samepassword = "Confirm password must same as password."
    
    
    public static let device_type = "2" // iOS = 2
}
