//
//  Validation.swift
//  SwiftDemo
//
//  Created by Redspark on 19/12/17.
//  Copyright © 2017 Redspark. All rights reserved.
//

import UIKit

let MAX_EMAIL_LENGTH = 50
let MIN_EMAIL_LENGTH = 5
let MAX_PASSWORD_LENGTH = 20
let MIN_PASSWORD_LENGTH = 6
let MAX_FIRSTNAME_LENGTH = 20

let MAX_MOBILE_LENGTH = 10
let MIN_MOBILE_LENGTH = 10

class Validation: NSObject
{
    
    func email(_ email: String) -> [Any] {
        if (email.count ) < 1 {
            return ["false", "Enter email address."]
        }
        else if (email.count) < MIN_EMAIL_LENGTH {
            return ["false", "Invalid email address."]
        }
        else if (email.count) > MAX_EMAIL_LENGTH {
            return ["false", "Invalid email address."]
        }
        else {
            let emailRegex = "[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
            var aRange: NSRange
            if emailTest.evaluate(with: email) {
                aRange = (email as NSString).range(of: ".", options: .backwards, range: NSRange(location: 0, length: (email.count )))
                let indexOfDot: Int = aRange.location
                //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
                if aRange.location != NSNotFound {
                    var topLevelDomain: String? = (email as NSString).substring(from: indexOfDot)
                    topLevelDomain = topLevelDomain?.lowercased()
                    //NSLog(@"topleveldomains:%@",topLevelDomain);
                    var TLD = Set<AnyHashable>()
                    TLD = Set<AnyHashable>([".aero", ".asia", ".biz", ".cat", ".com", ".coop", ".edu", ".gov", ".info", ".int", ".jobs", ".mil", ".mobi", ".museum", ".name", ".net", ".org", ".pro", ".tel", ".travel", ".ac", ".ad", ".ae", ".af", ".ag", ".ai", ".al", ".am", ".an", ".ao", ".aq", ".ar", ".as", ".at", ".au", ".aw", ".ax", ".az", ".ba", ".bb", ".bd", ".be", ".bf", ".bg", ".bh", ".bi", ".bj", ".bm", ".bn", ".bo", ".br", ".bs", ".bt", ".bv", ".bw", ".by", ".bz", ".ca", ".cc", ".cd", ".cf", ".cg", ".ch", ".ci", ".ck", ".cl", ".cm", ".cn", ".co", ".cr", ".cu", ".cv", ".cx", ".cy", ".cz", ".de", ".dj", ".dk", ".dm", ".do", ".dz", ".ec", ".ee", ".eg", ".er", ".es", ".et", ".eu", ".fi", ".fj", ".fk", ".fm", ".fo", ".fr", ".ga", ".gb", ".gd", ".ge", ".gf", ".gg", ".gh", ".gi", ".gl", ".gm", ".gn", ".gp", ".gq", ".gr", ".gs", ".gt", ".gu", ".gw", ".gy", ".hk", ".hm", ".hn", ".hr", ".ht", ".hu", ".id", ".ie", " No", ".il", ".im", ".in", ".io", ".iq", ".ir", ".is", ".it", ".je", ".jm", ".jo", ".jp", ".ke", ".kg", ".kh", ".ki", ".km", ".kn", ".kp", ".kr", ".kw", ".ky", ".kz", ".la", ".lb", ".lc", ".li", ".lk", ".lr", ".ls", ".lt", ".lu", ".lv", ".ly", ".ma", ".mc", ".md", ".me", ".mg", ".mh", ".mk", ".ml", ".mm", ".mn", ".mo", ".mp", ".mq", ".mr", ".ms", ".mt", ".mu", ".mv", ".mw", ".mx", ".my", ".mz", ".na", ".nc", ".ne", ".nf", ".ng", ".ni", ".nl", ".no", ".np", ".nr", ".nu", ".nz", ".om", ".pa", ".pe", ".pf", ".pg", ".ph", ".pk", ".pl", ".pm", ".pn", ".pr", ".ps", ".pt", ".pw", ".py", ".qa", ".re", ".ro", ".rs", ".ru", ".rw", ".sa", ".sb", ".sc", ".sd", ".se", ".sg", ".sh", ".si", ".sj", ".sk", ".sl", ".sm", ".sn", ".so", ".sr", ".st", ".su", ".sv", ".sy", ".sz", ".tc", ".td", ".tf", ".tg", ".th", ".tj", ".tk", ".tl", ".tm", ".tn", ".to", ".tp", ".tr", ".tt", ".tv", ".tw", ".tz", ".ua", ".ug", ".uk", ".us", ".uy", ".uz", ".va", ".vc", ".ve", ".vg", ".vi", ".vn", ".vu", ".wf", ".ws", ".ye", ".yt", ".za", ".zm", ".zw"])
                    
                    if topLevelDomain != nil && TLD.contains(topLevelDomain!) {
                        //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                        return ["true", ""]
                    }
                    else
                    {
                        return ["false", "Invalid email address."]
                    }
                    
                }
            }
            
            return ["false", "Invalid email address."]
        }
    }
    
    
    func loginPassword(_ password: String) -> [Any] {
        if (password.count ) < 1 {
            return ["false", "Enter password."]
        }
        else if (password.count ) < MIN_PASSWORD_LENGTH {
            return ["false", "Minimum \(MIN_PASSWORD_LENGTH) character password."]
        }
        else if (password.count) > MAX_PASSWORD_LENGTH {
            return ["false", "Maximum \(MAX_PASSWORD_LENGTH) character password."]
        }
        else {
            let whitespace = CharacterSet.whitespacesAndNewlines
            let range: NSRange = (password as NSString).rangeOfCharacter(from: whitespace)
            if range.location != NSNotFound {
                // There is whitespace.
                return ["false", "Space is not allowed in password."]
            }
        }
        
        return ["true", ""]
    }
    
    
    func password(_ password: String) -> [Any] {
        
        var upperCaseLetter: Bool = false
        var digit: Bool = false
        
        if password.count >= 8 {
            
            for char in password.unicodeScalars {
                if !upperCaseLetter {
                    upperCaseLetter = CharacterSet.uppercaseLetters.contains(char)
                }
                
                if !digit {
                    digit = CharacterSet.decimalDigits.contains(char)
                }
            }
            
            if  (!digit  && !upperCaseLetter) {
                return ["false", "Password must contain one upper case and at least number with 8 characters."]
            }
        }
        else{
            
            return ["false", "Password must contain one upper case and at least number with 8 characters."]
        }
        
        return ["true", ""]
        
    }
    
    func newPassword(_ password: String) -> [Any] {
        if (password.count ) < 1 {
            return ["false", "Enter new password."]
        }
        else if (password.count ) < MIN_PASSWORD_LENGTH {
            return ["false", "Minimum \(MIN_PASSWORD_LENGTH) character password."]
        }
        else if (password.count ) > MAX_PASSWORD_LENGTH {
            return ["false", "Maximum \(MAX_PASSWORD_LENGTH) character password."]
        }
        else {
            let whitespace = CharacterSet.whitespacesAndNewlines
            let range: NSRange = (password as NSString).rangeOfCharacter(from: whitespace)
            if range.location != NSNotFound {
                return ["false", "Space is not allowed in password."]
            }
        }
        
        return ["true", ""]
    }
    
    func fname(_ fname: String) -> [Any] {
        if (fname.count ) < 1 {
            return ["false", "Enter first name."]
        }
        else if (fname.count ) < 2 {
            return ["false", "Minimum 2 character in first name."]
        }
        else if (fname.count) > MAX_FIRSTNAME_LENGTH {
            return ["fname", "Maximum \(MAX_FIRSTNAME_LENGTH) character first name."]
        }
        else {
            let emailRegex = "^[a-z A-Z]*$"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
            if !emailTest.evaluate(with: fname) {
                return ["false", "Only alphabets in the first name."]
            }
        }
        
        return ["true", ""]
    }
    
    func lname(_ lname: String) -> [Any] {
        if (lname.count) < 1 {
            return ["false", "Enter last name."]
        }
        else if (lname.count) < 2 {
            return ["false", "Minimum 2 character in last name."]
        }
        else if (lname.count) > MAX_FIRSTNAME_LENGTH {
            return ["false", "Maximum \(MAX_FIRSTNAME_LENGTH) character last name."]
        }
        else {
            let emailRegex = "^[a-z A-Z]*$"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
            if !emailTest.evaluate(with: lname) {
                return ["false", "Only alphabets in the last name."]
            }
        }
        
        return ["true", ""]
    }
    
    func mobileNumber(_ Number: String) -> [Any] {
        let notDigits = CharacterSet.decimalDigits.inverted
        if (Number.count ) < 1 {
            return ["false", "Enter phone number."]
        }
        else if (Number as NSString).rangeOfCharacter(from: notDigits).location != NSNotFound || (Number.count ) < MIN_MOBILE_LENGTH || (Number.count ) > MAX_MOBILE_LENGTH {
            return ["false", "Invalid phone number."]
        }
        
        return ["true", ""]
    }
    
    func userName(_ channelusername: String) -> [Any] {
        let emailRegex = "^[a-zA-Z0-9_]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        if (channelusername.count ) < 1 {
            return ["false", "Enter username."]
        }
        else if (channelusername.count ) < 2 {
            return ["false", "Invalid username - Min. 2 characters."]
        }
        else if !emailTest.evaluate(with: channelusername) {
            return ["false", "Invalid username - Alphanumeric and underscore only."]
        }
        else if (channelusername.count ) > 20 {
            return ["false", "Invalid username - Max. 20 characters."]
        }
        
        return ["true", ""]
    }
}
